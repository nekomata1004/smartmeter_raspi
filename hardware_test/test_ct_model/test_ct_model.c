#include "../../smartmeter_lib/ct_model.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <json-c/json.h>



void meter_load_ct_profile(FILE **ct_file, const char *filename, ct_profile_t *ct_model);



int main()
{   
    ct_profile_t ct_model;
    FILE *ct_profile_file;
    meter_load_ct_profile(&ct_profile_file, "ct-profile-e4626-x101.json", &ct_model);
    
    
    double current = 10.0;
    double temperature = 25.0;

    printf("for temp, current = %lf, %lf\n", temperature, current);
    printf("phase error = %f\n", ct_get_phase_error(&ct_model, temperature, current));
    
    return 0;
}



void meter_load_ct_profile(FILE **ct_file, const char *filename, ct_profile_t *ct_model)
{
    char buffer[1024];
    struct json_object *parsed_json;
    struct json_object *ct_name;
    struct json_object *model_type;
    struct json_object *degree;
    struct json_object *coeffs;
    struct json_object *coeff;
    struct json_object *intercept;
    size_t n_coeffs;

    if(*ct_file == NULL){
        printf("CT profile file does not exist, exit read function\n");
    }
    else{
        *ct_file = fopen(filename, "r");
        fread(buffer, 1024, 1, *ct_file);
        fclose(*ct_file);

        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "ct_name", &ct_name);
        json_object_object_get_ex(parsed_json, "model_type", &model_type);
        json_object_object_get_ex(parsed_json, "degree", &degree);
        json_object_object_get_ex(parsed_json, "coeffs", &coeffs);
        json_object_object_get_ex(parsed_json, "intercept", &intercept);
        
	printf("CT Profile Loading...\n");
        printf("CT name: %s\n", json_object_get_string(ct_name));
        printf("Model type: %s\n", json_object_get_string(model_type));
        printf("Degree: %d\n", json_object_get_int(degree));

        n_coeffs = json_object_array_length(coeffs);
        printf("Coeff lenght =  %lu\n", n_coeffs);

	// Config CT model struct with loaded data
        ct_model->model_type = json_object_get_int(model_type);
        ct_model->degree = json_object_get_int(degree);
        ct_model->intercept = json_object_get_double(intercept);
        for(int i=0;i<n_coeffs;i++) {
                coeff = json_object_array_get_idx(coeffs, i);
                ct_model->coeffs[i] = json_object_get_double(coeff);
                printf("%d. %lf\n", i+1, json_object_get_double(coeff));
        }
    }
}

