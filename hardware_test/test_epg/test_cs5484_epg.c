#include "../../smartmeter_lib/cs5484_wiringpi.h"
#include "../../smartmeter_lib/relay_led.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <math.h>

/*
#define FULLSCALE_RAWDATA_VOLTAGE        0x999999
#define FULLSCALE_RAWDATA_CURRENT        0x999999
#define FULLSCALE_RAWDATA_POWER          0x2E147B
#define FULLSCALE_INPUT_VOLTAGE          0.250
#define R_DIVIDER_GAIN_INVERSE           2477
#define CT_RATIO                         2500
#define R_BURDEN                         5.1
#define FULLSCALE_OUTPUT_VOLTAGE         0.250 / sqrt(2) * R_DIVIDER_GAIN_INVERSE
#define FULLSCALE_OUTPUT_CURRENT         0.250 / sqrt(2) * CT_RATIO / R_BURDEN
#define FULLSCALE_OUTPUT_POWER           FULLSCALE_OUTPUT_VOLTAGE * FULLSCALE_OUTPUT_VOLTAGE
*/

void EPGInterruptCallback_KWH();
void EPGInterruptCallback_KVARH();

cs5484_basic_config_t   s_cs5484_config;
cs5484_input_config_t   s_cs5484_channel_1;
cs5484_input_config_t   s_cs5484_channel_2;
cs5484_epg_config_t     s_cs5484_epg;
relay_led_config_t      s_relay_config;

volatile int keepRunning = 1;
volatile int wiringpi_setup = 0;
void intHandler(int dummy){
    keepRunning = 0;
    printf("keyboard interrupt, send exit program...\n");

    /*
     * Disable all GPIOs before exist program
     *
     */
    if (wiringpi_setup){
        /*
	printf("Turn off GPIO of SPI before exit\n");
	pullUpDnControl(CS_PIN, PUD_OFF);
	digitalWrite(CS_PIN, 0);
	*/
    }
}

int main()
{  
    // signal handler initialize (for detect Ctrl + C exist)
    signal(SIGINT, intHandler);

    /*
     * CS5484 Basic Configuration
     *
     */
    s_cs5484_config.spi_bus_speed = 100000;
    s_cs5484_config.spi_cs_pin = CS_PIN;
    s_cs5484_config.spi_mosi_pin = MOSI_PIN;
    s_cs5484_config.spi_miso_pin = MISO_PIN;
    s_cs5484_config.spi_sck_pin = SCK_PIN;
    s_cs5484_config.chip_reset_pin = RST_PIN;
    s_cs5484_config.csum_en = 0;
    s_cs5484_config.conversion_type = CONVERSION_TYPE_CONTINUOUS;

    /*
     * CS5484 Input Configuration of Channel_1
     *
     */
    s_cs5484_channel_1.channel = 1;
    s_cs5484_channel_1.filter_mode_v = FILTER_MODE_HPF;
    s_cs5484_channel_1.filter_mode_i = FILTER_MODE_HPF;
    s_cs5484_channel_1.phase_error = 0;
    s_cs5484_channel_1.ac_offset_i = 0;
    s_cs5484_channel_1.offset_p = 0;
    s_cs5484_channel_1.offset_q = 0;
    
    /*
     * CS5484 Input Configuration of Channel_2
     *
     */
    s_cs5484_channel_2.channel = 2;
    s_cs5484_channel_2.filter_mode_v = FILTER_MODE_DISABLE;
    s_cs5484_channel_2.filter_mode_i = FILTER_MODE_DISABLE;
    s_cs5484_channel_2.phase_error = 0;
    s_cs5484_channel_2.ac_offset_i = 0;
    s_cs5484_channel_2.offset_p = 0;
    s_cs5484_channel_2.offset_q = 0;

    /*
     * Relay & Pulse LED config
     *
     */
    s_relay_config.relay_open_pin = PIN_RELAY_OPEN;
    s_relay_config.relay_close_pin = PIN_RELAY_CLOSE;
    s_relay_config.relay_state = RELAY_STATE_CONNECT;
    s_relay_config.led_kwh_pin = PIN_LED_KWH;
    s_relay_config.led_kvarh_pin = PIN_LED_KVARH;

    /*
     * CS5484 Energy Pulse Generation (EPG) Configuration
     *
     */
    s_cs5484_epg.kwh_pin = CS5484_EPG_IOPIN_KWH;
    s_cs5484_epg.kvarh_pin = CS5484_EPG_IOPIN_KVARH;
    s_cs5484_epg.freq_range  = 4;
    s_cs5484_epg.pulse_width = 3200;
    s_cs5484_epg.pulse_rate  = 0.187378444;    // for Umax (437V), Imax (86A), 800imp/kWh


    /*
     *  initialize wiring-Pi and SPI interface
     *
     */
    int ret; // for return status of function call
    wiringPiSetupGpio();
    wiringpi_setup = 1;
    pinMode(CS_PIN, OUTPUT);
    pinMode(RST_PIN, OUTPUT);
    pinMode(CS5484_EPG_IOPIN_KWH, INPUT);
    pinMode(CS5484_EPG_IOPIN_KVARH, INPUT);
    pullUpDnControl(CS_PIN, PUD_OFF);
    pullUpDnControl(RST_PIN, PUD_UP);
    pullUpDnControl(CS5484_EPG_IOPIN_KWH, PUD_OFF);
    pullUpDnControl(CS5484_EPG_IOPIN_KVARH, PUD_OFF);
    digitalWrite(CS_PIN, 1);
    digitalWrite(RST_PIN, 1);
    ret = wiringPiSPISetupMode(SPI_CHANNEL_0, SPI_BUS_SPEED, SPI_MODE_CS5484);
    printf("SPI init result : %d\r\n", ret);
    
    // Config GPIO for Relay & Pulse LED
    relay_led_gpio_init(&s_relay_config);

    // Test blinking pulse LED
    led_kwh_on(&s_relay_config);
    led_kvarh_on(&s_relay_config);
    delay(1500);
    led_kwh_off(&s_relay_config);
    led_kvarh_off(&s_relay_config);



    /*
     * CS5484 : Reset CMD
     *
     */
    digitalWrite(RST_PIN, 0);
    delay(100);
    digitalWrite(RST_PIN, 1);
    delay(100);
    cs5484_reset(&s_cs5484_config);
    printf("Reset CS5484 : ret=%d\r\n", ret);
    delay(10000);
    
    cs5484_page_select(&s_cs5484_config, 0);
    printf("change page, ret=%d\n", ret);
    
    
    
    /*
     * CS5484 : Test Write and Read back (Echo)
     *
     */
    uint32_t data_buf;
     
    // read Config0
    cs5484_reg_read(&s_cs5484_config, &data_buf, 0);
    printf("read Config0, default val = 0x400000, read value = %x\n", data_buf);
    delay(1000);
         
    // read Config1
    cs5484_reg_read(&s_cs5484_config, &data_buf, 1);
    printf("read Config1, default val = 0x00EEEE, read value = %x\n", data_buf);
    delay(1000);

    // read UART control
    cs5484_reg_read(&s_cs5484_config, &data_buf, 7);
    printf("read UART control, default val = 0x02004D, read value = %x\n", data_buf);
    delay(1000);

    // enable high-pass filter for DC removal 
    cs5484_input_filter_init(&s_cs5484_config, &s_cs5484_channel_1);

    /*
     * Energy Pulse Generation testing
     *
     */
    printf("Energy Pulse Generation Initialize ...\n...\n...\n");
    cs5484_epg_setup(&s_cs5484_config, &s_cs5484_epg);

    printf("Check config\n");
    delay(200);
    cs5484_page_select(&s_cs5484_config, 0);
    cs5484_reg_read(&s_cs5484_config, &data_buf, 8);
    printf("PulseWidth register, read value = %x\n", data_buf);
    delay(200);
    cs5484_page_select(&s_cs5484_config, 18);
    cs5484_reg_read(&s_cs5484_config, &data_buf, 28);
    printf("PulseRate register, read value = %x\n", data_buf);
    delay(200);
    cs5484_page_select(&s_cs5484_config, 0);
    cs5484_reg_read(&s_cs5484_config, &data_buf, 9);
    printf("PulseCtrl register, read value = %x\n", data_buf);
    delay(200);
    cs5484_page_select(&s_cs5484_config, 0);
    cs5484_reg_read(&s_cs5484_config, &data_buf, 1);
    printf("Config1, default val = 0x00EEEE, read value = %x\n", data_buf);
    delay(200);
    
    // GPIO interrupt for CS5484's EPG
    wiringPiISR(CS5484_EPG_IOPIN_KWH, INT_EDGE_FALLING, &EPGInterruptCallback_KWH);
    wiringPiISR(CS5484_EPG_IOPIN_KVARH, INT_EDGE_FALLING, &EPGInterruptCallback_KVARH);

    // Start continuous conversion
    cs5484_start_conversion(&s_cs5484_config);
    delay(5000);

    uint32_t voltage_raw, current_raw, power_raw, pf_raw;
    double v, i, p, q, pf;
    double temp;
    uint32_t timeout;

    while(keepRunning){    
        i  = cs5484_get_current_rms(&s_cs5484_config, &s_cs5484_channel_1);
        v  = cs5484_get_voltage_rms(&s_cs5484_config, &s_cs5484_channel_1);
        p  = cs5484_get_act_power_avg(&s_cs5484_config, &s_cs5484_channel_1);
        q  = cs5484_get_react_power_avg(&s_cs5484_config, &s_cs5484_channel_1);
	pf = cs5484_get_pf(&s_cs5484_config, &s_cs5484_channel_1);
        printf("AC Line Voltage, Current, P, Q, PF = %.3f, %.3f, %.3f, %.3f, %.3f\n", v, i, p, q, pf);
	delay(5000);
    }
    return 0;
}


void EPGInterruptCallback_KWH(){
    printf("********** KWH Pulse! **********\n");
    led_kwh_pulse(&s_relay_config);
}
void EPGInterruptCallback_KVARH(){
    printf("********** KVARH Pulse! **********\n");
    led_kvarh_pulse(&s_relay_config);
}
