/*
 * Example how to use SHT31 Humid&Temperature sensor with raspberry pi
 * Pi HAT pin connection
 * Sensor board | Pi
 *         SDA -> GPIO 2 (I2C1 SDA)
 *         SCL -> GPIO 3 (I2C1 SCL) 
 *
 */




#include "../../smartmeter_lib/sht31_pi.h"
#include <wiringPi.h>

int main() 
{
	int ret;

	// config for sht31 sensor
	sht31_config_t s_sht31;
	s_sht31.i2c_bus = "/dev/i2c-1";
	s_sht31.i2c_bus_speed = SHT31_I2C_BUS_SPEED;
	s_sht31.device_addr = 0x44;
	ret = sht31_config(&s_sht31);
	if(ret != SHT31_CONFIG_OK)
	{
            return -1;
	}

	// reading loop 
	while(1){
		ret = sht31_measure(&s_sht31);
		if(ret == SHT31_READ_OK)
		{
		    printf("temp=%fC, RH=%f\%\n", s_sht31.temp, s_sht31.humidity);
		}

		delay(1000);
	}

	return 0;
}
