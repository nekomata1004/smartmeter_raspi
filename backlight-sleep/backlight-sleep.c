#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
//#include <sys/time.h>
//#include <sys/types.h>



int main(){

        int fd;
        char buf[256];
        fd_set readfds;
        struct input_event event;
	struct timeval tv;

reinit:
	/* 
	 * Open input device
	 */
        fd = open("/dev/input/mice", O_RDONLY);
        if(fd == -1){
                printf("open_port: cannot open file!\n");
        }

        /* 
	 * Set fd to wait for input
	 */
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);

	/*
	 * Touch check loop
	 * For sleep and wakeup scheduling
	 *
	 */
	int touch_state;
	int count;
        //ssize_t ret;
	int ret;
	tv.tv_sec = 61;
	tv.tv_usec = 0;
        while(1){
                ret = select(fd+1, &readfds, NULL, NULL, &tv);
                if(ret > 0){
                        //ret = read(fd, &event, sizeof(event));
			printf("touch is detected!\n");
			system("echo 255 | sudo tee /sys/class/backlight/rpi_backlight/brightness");
			goto reinit;
                }
		else{
			printf("touch timeout...\n");
			system("echo 0 | sudo tee /sys/class/backlight/rpi_backlight/brightness");
			goto reinit;
		}
		sleep(0.1);
        }

        return 0;
}

