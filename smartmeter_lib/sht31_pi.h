#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdint.h>


#define SHT31_I2C_BUS_SPEED 10000

enum sht31_result_t {
	SHT31_CONFIG_OK,
	SHT31_CONFIG_FAIL_BUS,
	SHT31_READ_ERROR,
	SHT31_READ_OK
};

/* 
 * Struct for sht31 config
 * Used to measure CT temperature
 *
 */
typedef struct
{
    char     *i2c_bus;
    uint32_t i2c_bus_speed;
    uint8_t  device_addr;
    int      file;
    double   temp;
    double   humidity;

}sht31_config_t;



int sht31_config(sht31_config_t *s_sht31);
int sht31_measure(sht31_config_t *s_sht31);

