#include "sht31_pi.h"



int sht31_config(sht31_config_t *s_sht31)
{
	// Create I2C bus
	if((s_sht31->file = open(s_sht31->i2c_bus, O_RDWR)) < 0) 
	{
		printf("Failed to open the bus. \n");
		return SHT31_CONFIG_FAIL_BUS;
	}
	// Get I2C device, SHT31 I2C default address is 0x44(68)
	ioctl(s_sht31->file, I2C_SLAVE, s_sht31->device_addr);

	return SHT31_CONFIG_OK;
}

int sht31_measure(sht31_config_t *s_sht31)
{
	// Send high repeatability measurement command
	// Command msb, command lsb(0x2C, 0x06)
	char config[2] = {0};
	config[0] = 0x2C;
	config[1] = 0x06;
	write(s_sht31->file, config, 2);
	
	// Read 6 bytes of data
	// temp msb, temp lsb, temp CRC, humidity msb, humidity lsb, humidity CRC
	char data[6] = {0};
	if(read(s_sht31->file, data, 6) != 6)
	{
		printf("Error : Input/output Error \n");
		return SHT31_READ_ERROR;
	}
	else
	{
		// Convert the data
		s_sht31->temp     = (((data[0] * 256) + data[1]) * 175.0) / 65535.0  - 45.0;
		s_sht31->humidity = (((data[3] * 256) + data[4])) * 100.0 / 65535.0;
		return SHT31_READ_OK;
	}
}
