#include "ct_model.h"



static double dot_product(double v1[], double v2[], int size_of_vector)
{
    double sum=0;
    for(int i=0; i<size_of_vector; i++)
    {
        sum = sum + v1[i]*v2[i];
    }

    return sum;
}

double ct_get_phase_error(ct_profile_t *ct_profile, double temperature, double current)
{
    /*
     * phase can be calculated by dot.product : y = a.x + b
     * where y is phase_err, x is feauture vector in form of ct_model[0].get_feature_names_out() in sklearn's model
     * a is coeff vector of polynomial model, b is intercept of linear model
     */

    double phase_err;
    double x[20];
    double a[20];

    if(ct_profile->model_type == CT_MODELTYPE_LINEAR_REGRESSION)
    {
	// coeff vector
	a[0] = 0;
	a[1] = ct_profile->coeffs[0];
	a[2] = ct_profile->coeffs[1];

	// feature vector
        x[0] = 1;
        x[1] = temperature;
        x[2] = current;

	phase_err = dot_product(a, x, 3) + ct_profile->intercept;
    }

    if(ct_profile->model_type == CT_MODELTYPE_POLYNOMIAL_REGRESSION)
    {
	if(ct_profile->degree == 4)
	{   
	    // coeff vector
	    a[0] = ct_profile->coeffs[0];
	    a[1] = ct_profile->coeffs[1];
	    a[2] = ct_profile->coeffs[2];
	    a[3] = ct_profile->coeffs[3];
	    a[4] = ct_profile->coeffs[4];
	    a[5] = ct_profile->coeffs[5];
	    a[6] = ct_profile->coeffs[6];
	    a[7] = ct_profile->coeffs[7];
	    a[8] = ct_profile->coeffs[8];
	    a[9] = ct_profile->coeffs[9];
	    a[10] = ct_profile->coeffs[10];
	    a[11] = ct_profile->coeffs[11];
	    a[12] = ct_profile->coeffs[12];
	    a[13] = ct_profile->coeffs[13];
	    a[14] = ct_profile->coeffs[14];
	        
	    // feature vector
	    x[0] = 1;
	    x[1] = temperature;
	    x[2] = current;
	    x[3] = pow(temperature, 2);
	    x[4] = temperature * current;
	    x[5] = pow(current, 2);
	    x[6] = pow(temperature, 3);
	    x[7] = pow(temperature, 2) * current;
	    x[8] = pow(current, 2) * temperature;
	    x[9] = pow(current, 3);
	    x[10] = pow(temperature, 4);
	    x[11] = pow(temperature, 3) * current;
	    x[12] = pow(temperature, 2) * pow(current, 2);
	    x[13] = pow(current, 3) * temperature;
	    x[14] = pow(current, 4);
            phase_err = dot_product(a, x, 15) + ct_profile->intercept;
	}
    }

    return phase_err;
}

