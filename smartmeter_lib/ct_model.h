#ifndef CT_MODEL_H
#define CT_MODEL_H

/*
 * Current Transformer (CT) Model
 * Used for phase error compensation due to temperature change and current range in CT
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <math.h>

#define CT_MODELTYPE_LINEAR_REGRESSION     0
#define CT_MODELTYPE_POLYNOMIAL_REGRESSION 1
#define CT_MODELTYPE_NEURALNET             2



/* 
 * Struct for CT profile
 *
 */
typedef struct
{
    int8_t      model_type;
    uint8_t     degree;
    double      coeffs[20];
    double      intercept;

}ct_profile_t;

double ct_get_phase_error(ct_profile_t *ct_profile, double temperature, double current);

#endif
