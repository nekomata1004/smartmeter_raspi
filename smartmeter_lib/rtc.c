#include "rtc.h"



int rtc_getTime(char *strTime)
{
    struct timeval tv;
    struct tm *tm;
    char buffer[80];

    // Get current time
    gettimeofday(&tv, NULL);
    // Convert to local time
    tm = localtime(&tv.tv_sec);
    
    // ISO8601
    // Format the date and time without milliseconds
    strftime(buffer, sizeof(buffer), "%Y-%m-%dT%H:%M:%S", tm);
    // Append milliseconds
    sprintf(buffer + strlen(buffer), ".%03ldZ", tv.tv_usec / 1000);
    
    //copy buffer to output time string
    strcpy(strTime, buffer);
    
    return 0;
}

int rtc_setTime()
{    
    return 0; //if success
}

int rtc_tickStart(rtc_tick_t *s_tick)
{
    clock_gettime(CLOCK_MONOTONIC, &(s_tick->ts));
    s_tick->tick_start = (uint64_t)s_tick->ts.tv_sec * 1000000000 + (uint64_t)s_tick->ts.tv_nsec;
    return 0;
}

int rtc_tickEnd(rtc_tick_t *s_tick)
{
    clock_gettime(CLOCK_MONOTONIC, &(s_tick->ts));
    s_tick->tick_end = (uint64_t)s_tick->ts.tv_sec * 1000000000 + (uint64_t)s_tick->ts.tv_nsec;
    return 0;
}

double rtc_getPerformTime(rtc_tick_t *s_tick)
{
    double time_perform;
    time_perform = (double) (s_tick->tick_end - s_tick->tick_start) / 1000000000;

    return time_perform;
}
