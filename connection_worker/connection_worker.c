#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <hiredis/hiredis.h>
#include "../smartmeter_lib/rtc.h"

#define DEBUG_WORKER   0
#if DEBUG_WORKER
#define REDIS_URL	    "127.0.0.1"
#else
#define REDIS_URL	    "redis"
#endif
#define REDIS_PORT	    6379

#define CONNECTION_STATUS_CONNECTED    1
#define CONNECTION_STATUS_DISCONNECTED 0

const char *REDIS_CHANNELNAME_CONNECTSTATUS = "smartmeter-raspi.meter-1phase-01.connect-status";



int main()
{
    printf("Connection Status Worker Start! Wait for 30sec...\n");
    sleep(30);

    /*
     * REDIS Sync Initialize
     * This client will be used for Redis's "SET" command
     *
     */
    redisContext* c = redisConnect((char*)REDIS_URL, REDIS_PORT);
    
    // REDIS authentication
    const char* command_auth = "auth ictadmin";
    redisReply* r = (redisReply*)redisCommand(c, command_auth);



    int ping_ret;
    int connection_status;
    while(1){
        /* 
	 * Use Ping to google DNS to check network connection
	 *
	 */  
        ping_ret = system("ping -c1 -w1 8.8.8.8 > /dev/null 2>&1");
        if(ping_ret == 0){
            printf("Ping Success, Online\n");
	    connection_status = CONNECTION_STATUS_CONNECTED;
	}
        else{
            printf("Ping Failed, Offline\n");
	    connection_status = CONNECTION_STATUS_DISCONNECTED;
	}



        /*
	 *  Read timestamp
	 *
	 */
        char timestamp[30];
	rtc_getTime(timestamp);
 


        /*
	 *  REDIS Set value with a CS5484's sampling
	 *
	 */
        redisFree(c);
        c = redisConnect((char*)REDIS_URL, REDIS_PORT);
	if(!(c->err)){
	    freeReplyObject(r);
            r = (redisReply*)redisCommand(c, command_auth);
	    if((r->type == REDIS_REPLY_STATUS) && (strcasecmp(r->str, "OK") == 0)){
	        char *channel;
	    	char value[20];
                printf("Set connection status via Redis...\n\n");
                
		channel = (char *)REDIS_CHANNELNAME_CONNECTSTATUS;
	    	sprintf(value, "%d", connection_status);
		freeReplyObject(r);
            	r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	    }        
        }
	else{
            printf("Cannot connect to redis server\n");
	}

	sleep(10);
    }
    return 0;
}


