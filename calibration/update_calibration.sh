#!/bin/sh

export SMARTMETER_PATH=/home/pi/smartmeter
echo "List of calibration files in smartmeter/measurement..."
ls $SMARTMETER_PATH/measurement
echo "copy calibration file to smartmeter/measurement"
cp calibration_output.txt $SMARTMETER_PATH/measurement
