#include "../smartmeter_lib/cs5484_wiringpi.h"
#include "../smartmeter_lib/ct_model.h"
#include "../smartmeter_lib/rtc.h"
#include "../smartmeter_lib/relay_led.h"
#include "../smartmeter_lib/sht31_pi.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>
#include <hiredis/adapters/libevent.h>
#include <pthread.h>
#include <json-c/json.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>


// Flag for worker debugging
#define DEBUG_WORKER   0
#define DATALOGGING_TEMPERATURE_COMPENSATION_TEST    0
#define DATALOGGING_ENERGY_PULSE_TEST                0

// External Temperature sensor : SHT31
#define USE_SHT31   1

// Energy Pulse Generation Block in cs5484
#define USE_EPG     1
/* 
 * Struct for Smartmeter's Tags 
 * 
 */
typedef struct
{
    float i;		// current
    float v;		// voltage
    float p;		// active power
    float q;            // reactive power
    float s;            // apparent power
    float pf; 	        // power factor
    double kwh;         // energy consumption (real)
    double kvarh;       // energy consumption (complex)
    double wh;          // energy consumption (real)
    double varh;        // energy consumption (complex)
    int relay_state;    // current state of latching relay
    int internet_status;    // internet status, connected or disconnected

}meter_tag_t;



#if DEBUG_WORKER
const char* BACKUP_FILENAME = "../backup_meter.txt";
const char* PULSE_LOG_FILENAME = "../pulse_log.txt";
const char* RELAY_POLARITY_FILENAME = "../relay_polarity.json";
const char* CALIBRATION_FILENAME = "../calibration/calibration_output.txt";
const char* CT_PROFILE_FILENAME  = "../calibration/ct-profile-e4624-x101.json";
#else
const char* BACKUP_FILENAME = "../data/backup_meter.txt";
const char* PULSE_LOG_FILENAME = "../data/pulse_log.txt";
const char* RELAY_POLARITY_FILENAME = "../data/relay_polarity.json";
const char* CALIBRATION_FILENAME = "../data/calibration_output.txt";
const char* CT_PROFILE_FILENAME  = "../data/ct-profile-e4624-x101.json";
#endif

const char* REDIS_CHANNELNAME_V     = "smartmeter-raspi.meter-1phase-01.v";
const char* REDIS_CHANNELNAME_I     = "smartmeter-raspi.meter-1phase-01.i";
const char* REDIS_CHANNELNAME_P     = "smartmeter-raspi.meter-1phase-01.P";
const char* REDIS_CHANNELNAME_Q     = "smartmeter-raspi.meter-1phase-01.Q";
const char* REDIS_CHANNELNAME_S     = "smartmeter-raspi.meter-1phase-01.S";
const char* REDIS_CHANNELNAME_PF    = "smartmeter-raspi.meter-1phase-01.PF";
const char* REDIS_CHANNELNAME_KWH   = "smartmeter-raspi.meter-1phase-01.KWH";
const char* REDIS_CHANNELNAME_KVARH = "smartmeter-raspi.meter-1phase-01.KVARH";
const char* REDIS_CHANNELNAME_RELAY_WRITE = "smartmeter-raspi.meter-1phase-01.relay_write";
const char* REDIS_CHANNELNAME_KWH_LOG   = "smartmeter-raspi.meter-1phase-01.KWH_log";
const char* REDIS_CHANNELNAME_KVARH_LOG = "smartmeter-raspi.meter-1phase-01.KVARH_log";
const char *REDIS_CHANNELNAME_INTERNET_STATUS = "smartmeter-raspi.meter-1phase-01.connect-status";

#if DEBUG_WORKER
#define REDIS_URL	    "127.0.0.1"
#else
#define REDIS_URL	    "redis"
#endif
#define REDIS_PORT	    6379

#define STATUS_READ_BACKUP_OK      0
#define STATUS_READ_BACKUP_ERROR   1
#define INTERNET_STATUS_CONNECTED    1
#define INTERNET_STATUS_DISCONNECTED 0

#define TEMPERATURE_LOG_FILENAME  "temperature_log.txt"

// Meter Constant
#define METER_CONSTANT      1000
#define PULSE_PER_KWH       METER_CONSTANT
#define PULSE_PER_KVARH     METER_CONSTANT
#define PULSE_PER_WH        ((double)PULSE_PER_KWH/1000.0)
#define PULSE_PER_VARH      ((double)PULSE_PER_KVARH/1000.0)
#define ENERGY_DIFF_MUL     1000000



/*
 * Meter functions
 *
 */
int meter_wiringpi_init(cs5484_basic_config_t *s_config, relay_led_config_t *s_relay_config, cs5484_epg_config_t *s_epg);
int meter_chipmeasurement_init(cs5484_basic_config_t *s_cs5484_config,
		               cs5484_input_config_t *s_cs5484_channel_1,
			       cs5484_input_config_t *s_cs5484_channel_2,
			       cs5484_epg_config_t   *s_cs5484_epg);
int meter_read_backup_file(FILE **backup_file, const char *filename, double *field, int n_field);
void meter_read_relay_polarity(FILE **relay_config_file, const char *filename, int *relay_polarity);
void meter_load_ct_profile(FILE **ct_file, const char *filename, ct_profile_t *ct_model);
void meter_load_calibration(FILE **calibration_file, const char *filename, 
		            cs5484_input_config_t *s_cs5484_channel_1);
int meter_check_chipconfig(cs5484_basic_config_t *s_cs5484_config, uint32_t ref_config0);

/*
 * Callback functions as interrupt service routine
 *
 */
// Redis callback
void connectCallback(const redisAsyncContext *c, int status);
void disconnectCallback(const redisAsyncContext *c, int status);
void onMessage(redisAsyncContext * c, void *reply, void * privdata);
void getCallback(redisAsyncContext *c, void *r, void *privdata);
// RPi IO callback
void EPGInterruptCallback_KWH();
void EPGInterruptCallback_KVARH();

/*
 * Configuration struct for measurement HAT board
 *
 */ 
cs5484_basic_config_t   s_cs5484_config;
cs5484_input_config_t   s_cs5484_channel_1;
cs5484_input_config_t   s_cs5484_channel_2;
cs5484_epg_config_t     s_cs5484_epg;
ct_profile_t            s_ct_profile;
relay_led_config_t      s_relay_config;
meter_tag_t             s_tag;

/*
 * Struct for SHT31 temperature sensor
 *
 */
sht31_config_t s_sht31;

/*
 * Global var for meter
 *
 */
volatile int redis_connected = 0;
volatile int redis_async_connected = 0;
volatile int sht31_run_flag = 0;
volatile int meter_run_flag = 0;
uint32_t meter_user_config0;
uint32_t meter_user_config1;
uint32_t meter_user_config2;
volatile uint32_t pulse_cnt_kwh;
volatile uint32_t pulse_cnt_kvarh;



/*
 *
 * Task : thread_measurement()
 * Description : Do measurement and set tag to Redis
 *
 */
void *thread_measurement(void *arg)
{  
    int ret;
    int conversion_error_count = 0;
    int measure_first_sample_flag = 0;
    rtc_tick_t s_timer;
    double timeInterval;
    double temperature;
    double phase_error;
    int phase_compensate_tick=0;
    uint32_t tick_settag=0;

    /*
     *  Read the previous value from backup file
     *
     */
    FILE *backup_file;
    const int n_meter_backup = 9; // i, v, p, q, s, pf, kwh, kvarh, relay_state
    double field[n_meter_backup];
    meter_read_backup_file(&backup_file, BACKUP_FILENAME, field, n_meter_backup);

    uint32_t buf;
    
    // assign backup value to current value
    s_tag.i = field[0];
    s_tag.v = field[1];
    s_tag.p = field[2];
    s_tag.q = field[3];
    s_tag.s = field[4];
    s_tag.pf = field[5];
    s_tag.kwh = field[6];
    s_tag.kvarh = field[7];
    s_tag.wh = s_tag.kwh * 1000;
    s_tag.varh = s_tag.kvarh * 1000;
    s_tag.relay_state = (int)field[8];
    s_tag.internet_status = INTERNET_STATUS_DISCONNECTED;
    printf("Read backup value, I, V, P, Q, S, PF, KWH, Relay_State :\t");
    printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%d\r\n", s_tag.i, s_tag.v, s_tag.p, s_tag.q, s_tag.s, s_tag.pf, s_tag.kwh, s_tag.relay_state);

    /*
     * CS5484 Basic Configuration
     * Raspi
     */
    s_cs5484_config.pi_spi_channel = SPI_CHANNEL_0;
    s_cs5484_config.spi_bus_speed  = SPI_BUS_SPEED;
    s_cs5484_config.spi_mode       = SPI_MODE_CS5484;
    s_cs5484_config.spi_cs_pin     = CS_PIN;
    s_cs5484_config.spi_mosi_pin   = MOSI_PIN;
    s_cs5484_config.spi_miso_pin   = MISO_PIN;
    s_cs5484_config.spi_sck_pin    = SCK_PIN;
    s_cs5484_config.chip_reset_pin = RST_PIN;
    s_cs5484_config.csum_en = 0;
    s_cs5484_config.conversion_type = CONVERSION_TYPE_CONTINUOUS;

    /*
     * CS5484 Input Configuration of Channel_1
     *
     */
    s_cs5484_channel_1.channel = 1;
    s_cs5484_channel_1.filter_mode_v = FILTER_MODE_HPF;
    s_cs5484_channel_1.filter_mode_i = FILTER_MODE_HPF;
    s_cs5484_channel_1.phase_error = 0;
    s_cs5484_channel_1.ac_offset_i = 0;
    s_cs5484_channel_1.offset_p = 0;
    s_cs5484_channel_1.offset_q = 0;
    
    /*
     * CS5484 Input Configuration of Channel_2
     *
     */
    s_cs5484_channel_2.channel = 2;
    s_cs5484_channel_2.filter_mode_v = FILTER_MODE_DISABLE;
    s_cs5484_channel_2.filter_mode_i = FILTER_MODE_DISABLE;
    s_cs5484_channel_2.phase_error = 0;
    s_cs5484_channel_2.ac_offset_i = 0;
    s_cs5484_channel_2.offset_p = 0;
    s_cs5484_channel_2.offset_q = 0;

    /*
     *  Relay & LED Configuration
     *
     */
    FILE *relay_config_file;
    int relay_polarity;
    meter_read_relay_polarity(&relay_config_file, RELAY_POLARITY_FILENAME, &relay_polarity);
    if(relay_polarity == 0){
        s_relay_config.relay_open_pin = PIN_RELAY_OPEN; 
        s_relay_config.relay_close_pin = PIN_RELAY_CLOSE;
    }
    else{
        s_relay_config.relay_open_pin = PIN_RELAY_CLOSE; 
        s_relay_config.relay_close_pin = PIN_RELAY_OPEN;
    }
    s_relay_config.relay_state = s_tag.relay_state; 
    s_relay_config.led_kwh_pin = PIN_LED_KWH; 
    s_relay_config.led_kvarh_pin = PIN_LED_KVARH;
    
    /*
     * CS5484 Energy Pulse Generation (EPG) Configuration
     *
     */
    s_cs5484_epg.kwh_pin = CS5484_EPG_IOPIN_KWH;
    s_cs5484_epg.kvarh_pin = CS5484_EPG_IOPIN_KVARH;
    s_cs5484_epg.pulse_width = 1600;                // pulse width is about 25ms
    s_cs5484_epg.meter_constant = METER_CONSTANT;

    /*
     *  Initialize wiring-Pi and SPI interface, CS5484 Chip, Load CT Profile and Calibration file
     *
     */
    FILE *calibration_file;
    FILE *calibration_ct_phase_file;
    FILE *ct_file;
    meter_wiringpi_init(&s_cs5484_config, &s_relay_config, &s_cs5484_epg);
    meter_load_calibration(&calibration_file, CALIBRATION_FILENAME, &s_cs5484_channel_1);
    meter_load_ct_profile(&ct_file, CT_PROFILE_FILENAME, &s_ct_profile);
    // Warning : meter_chipmeasurement_init() function must be called at last after read all files (backup, calibration, profile etc.)
    meter_chipmeasurement_init(&s_cs5484_config, &s_cs5484_channel_1, &s_cs5484_channel_2, &s_cs5484_epg);
    
    /*
     * Set phase conpensation at first time
     *
     */
#if USE_SHT31 == 1
    printf("wait for sht31...\n");
    int sht31_timeout = 0;
    while(sht31_run_flag == 0){
	// wait until sht31 start to run
	sleep(1);
	sht31_timeout++;
        if(sht31_timeout >= 5)
	{
	    // read start temperature from cs5484 instead
	    cs5484_start_conversion(&s_cs5484_config);
            cs5484_wait_for_conversion(&s_cs5484_config, 200);
	    s_sht31.temp = cs5484_get_temperature(&s_cs5484_config);
	    break;
	}
    }
    temperature = s_sht31.temp;
#else
    temperature = cs5484_get_temperature(&s_cs5484_config)
#endif

    phase_error = ct_get_phase_error(&s_ct_profile, temperature, s_tag.i);
    s_cs5484_channel_1.phase_error = -1.0 * phase_error + s_cs5484_channel_1.phase_offset_manual;
    cs5484_set_phase_compensation(&s_cs5484_config, &s_cs5484_channel_1);
    printf("Read temperature = %lf Celsius, phase error = %lf degree\n", temperature, phase_error);



    /*
     * REDIS Sync Initialize
     * This client will be used for Redis's "SET" command
     *
     */
    redisContext* c = redisConnect((char*)REDIS_URL, REDIS_PORT);
    
    // REDIS authentication
    const char* command_auth = "auth ictadmin";
    redisReply* r = (redisReply*)redisCommand(c, command_auth);



    /*
     * main loop
     *
     */
    meter_run_flag = 1;  // start measurement
    cs5484_start_conversion(&s_cs5484_config);

    while(meter_run_flag)
    {  
	sleep(1);

	/*
	 *  Wait until config0 of cs5484 is corrected
	 */
        while(meter_check_chipconfig(&s_cs5484_config, meter_user_config0) != STATUS_OK)
	{   
            meter_chipmeasurement_init(&s_cs5484_config, &s_cs5484_channel_1, &s_cs5484_channel_2, &s_cs5484_epg);
	    sleep(0.1);
	}

	// read all param from conversion result
        s_tag.i  = cs5484_get_current_rms(&s_cs5484_config, &s_cs5484_channel_1);
        s_tag.v  = cs5484_get_voltage_rms(&s_cs5484_config, &s_cs5484_channel_1);
        s_tag.p  = cs5484_get_act_power_avg(&s_cs5484_config, &s_cs5484_channel_1);
        s_tag.q  = cs5484_get_react_power_avg(&s_cs5484_config, &s_cs5484_channel_1);
        s_tag.s  = cs5484_get_apparent_power_avg(&s_cs5484_config, &s_cs5484_channel_1);
	s_tag.pf = cs5484_get_pf(&s_cs5484_config, &s_cs5484_channel_1);
            
	// measure the time interval between previous sampling and current sampling 
	if(measure_first_sample_flag == 0)
	{
	    timeInterval = 1.0;
	    measure_first_sample_flag = 1;
	}
	else
	{
	    rtc_tickEnd(&s_timer); // stop tick
	    timeInterval = rtc_getPerformTime(&s_timer);
	}
	rtc_tickStart(&s_timer); // re-start tick



#if USE_EPG == 0
	// Calculate energy kWh, kVARh
	double dwh;
	double dvarh;
        dwh = (double)s_tag.p/3600.0 * timeInterval;
        dvarh = (double)s_tag.q/3600.0 * timeInterval;
	s_tag.wh   = s_tag.wh + dwh;
	s_tag.varh = s_tag.varh + dvarh; 
        s_tag.kwh  = s_tag.kwh + dwh/1000.0;
	s_tag.kvarh = s_tag.kvarh + dvarh/1000.0;
#endif

	

	/*
	 *  Read temperature & Phase compensate section
	 *
	 */
#if USE_SHT31
        // Read from SHT31
        temperature = s_sht31.temp;

	// Phase compensation for about every 10 seconds
	phase_compensate_tick++;
	if(phase_compensate_tick >= 10)
	{
	    // Read how much CT's phase error
            phase_error = ct_get_phase_error(&s_ct_profile, temperature, s_tag.i);
	    printf("******************\n**************** current phase error = %lf\n", phase_error);

	    // Update phase error and set compensated value
            s_cs5484_channel_1.phase_error = -1.0 * phase_error + s_cs5484_channel_1.phase_offset_manual;
            cs5484_set_phase_compensation(&s_cs5484_config, &s_cs5484_channel_1);

            // reset tick
            phase_compensate_tick = 0;
	}
#else
	// Checking if temperature config fail
	int timeout=0;
	while(!cs5484_is_temperature_ready(&s_cs5484_config) && !(timeout > 2000)){
	    // wait for TUP, do nothing
	    // It should be better if interrupt is used in this aplication.
	    sleep(0.01);
	    timeout++;
	}
	if(timeout > 2000){
	    // reinitialize chip
            printf("chip config lost! re-init chip...\n");
            meter_chipmeasurement_init(&s_cs5484_config, &s_cs5484_channel_1, &s_cs5484_channel_2, &s_cs5484_epg);
	}
	else{
	    // Read temperature if config is still OK
	    temperature = cs5484_get_temperature(&s_cs5484_config);

	    // Phase compensation for about every 30 seconds
	    phase_compensate_tick++;
            if(phase_compensate_tick >= 30)
	    {
	        // Read how much CT's phase error
                phase_error = ct_get_phase_error(&s_ct_profile, temperature, s_tag.i);
		printf("******************\n**************** current phase error = %lf\n", phase_error);
            
		// Update phase error and set compensated value
                s_cs5484_channel_1.phase_error = -1.0 * phase_error + s_cs5484_channel_1.phase_offset_manual;
                cs5484_set_phase_compensation(&s_cs5484_config, &s_cs5484_channel_1);

		// reset tick
		phase_compensate_tick = 0;
	    }
	}
#endif
 
	// print result
        printf("meter temperature :%f\n", temperature);
        printf("I, V, P, Q, S, PF, KWH :\t");
	printf("%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\r\n", s_tag.i, s_tag.v, s_tag.p, s_tag.q, s_tag.s, s_tag.pf, s_tag.kwh);

	/*
	 *  Read timestamp
	 *
	 */
        char timestamp[30];
	rtc_getTime(timestamp);
        printf("timestamp=%s\n", timestamp);

	/*
	 *  Write data to backup file
	 *
	 */
        backup_file = fopen(BACKUP_FILENAME, "r+");
	if(backup_file != NULL){
            char str_buf[200];
	    sprintf(str_buf, "%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%f,%f,%.1f,timestamp=%s\n", \
	            s_tag.i, s_tag.v, s_tag.p, s_tag.q, s_tag.s, s_tag.pf, s_tag.kwh, s_tag.kvarh, (float)s_tag.relay_state, timestamp);

	    fputs(str_buf, backup_file);
	    fclose(backup_file);
	}
	
	
#if DATALOGGING_TEMPERATURE_COMPENSATION_TEST
        FILE *temperature_log_file;
	/*
	 *  Stamp blinking to log file
	 *
	 */
        temperature_log_file = fopen(TEMPERATURE_LOG_FILENAME, "a");
	if(temperature_log_file != NULL){
           char str_buf[200];
	   sprintf(str_buf, "%s,%.3f,%.3f,%.3f\n", \
		   timestamp, temperature, s_tag.i, s_tag.pf);

	   fputs(str_buf, temperature_log_file);
	   fclose(temperature_log_file);
        }
#endif

	
	/*
	 *  REDIS Set value with a CS5484's sampling
	 *
	 */
        
        redisFree(c);
        c = redisConnect((char*)REDIS_URL, REDIS_PORT);
	if(!(c->err)){
	    freeReplyObject(r);
            r = (redisReply*)redisCommand(c, command_auth);
	    if((r->type == REDIS_REPLY_STATUS) && (strcasecmp(r->str, "OK") == 0)){
	        char *channel;
	    	char value[20];
                printf("Set tag via Redis...\n\n"); 
	        
                if(tick_settag % 5 == 0)
		{
	    	    channel = (char *)REDIS_CHANNELNAME_V;
	    	    sprintf(value, "%.3f", s_tag.v);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	    	    channel = (char *)REDIS_CHANNELNAME_I;
	    	    sprintf(value, "%.3f", s_tag.i);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	   
	    	    channel = (char *)REDIS_CHANNELNAME_P;
	    	    sprintf(value, "%.3f", s_tag.p/1000);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	        
	    	    channel = (char *)REDIS_CHANNELNAME_Q;
	    	    sprintf(value, "%.3f", s_tag.q/1000);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);

	    	    channel = (char *)REDIS_CHANNELNAME_S;
	    	    sprintf(value, "%.3f", s_tag.s/1000);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	        
		    channel = (char *)REDIS_CHANNELNAME_PF;
	    	    sprintf(value, "%.2f", s_tag.pf);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
		}
                if(tick_settag % 10 == 0)
		{	
	    	    channel = (char *)REDIS_CHANNELNAME_KWH;
	    	    sprintf(value, "%.3f", s_tag.kwh);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	    	
		    channel = (char *)REDIS_CHANNELNAME_KVARH;
	    	    sprintf(value, "%.3f", s_tag.kvarh);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
		
		    channel = (char *)REDIS_CHANNELNAME_RELAY_WRITE;
	     	    sprintf(value, "%d", (int)s_tag.relay_state);
	 	    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
		   								     , channel, timestamp);

		    channel = (char *)REDIS_CHANNELNAME_INTERNET_STATUS;
	     	    sprintf(value, "%d", (int)s_tag.internet_status);
	 	    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
		   								     , channel, timestamp);
		}
		if(tick_settag % 900 == 0) // tag for scada's log
		{
	    	    channel = (char *)REDIS_CHANNELNAME_KWH_LOG;
	    	    sprintf(value, "%f", s_tag.kwh);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
	    	
		    channel = (char *)REDIS_CHANNELNAME_KVARH_LOG;
	    	    sprintf(value, "%f", s_tag.kvarh);
		    freeReplyObject(r);
            	    r = (redisReply*)redisCommand(c, "%s tag:%s %s tagtime:%s %s", "mset", channel, value
										     , channel, timestamp);
		}

		tick_settag++;
	    }
	    else{
		printf("Cannot auth to redis\n");
	    }
        }
	else{
            printf("Cannot connect to redis server\n");
	}
    }
}

/*
 *
 * Task : thread_sht31_temperature()
 * Description : Read temperature from sht31 sensor for CT phase compensation
 *
 */
void *thread_sht31_temperature(void *arg)
{
    // config for sht31 sensor
    sht31_run_flag = 0;
    s_sht31.i2c_bus = "/dev/i2c-1";
    s_sht31.i2c_bus_speed = SHT31_I2C_BUS_SPEED;
    s_sht31.device_addr = 0x44;

    while(sht31_config(&s_sht31) != SHT31_CONFIG_OK){
        sleep(0.1);
    }
    
    // wait until measurement is running
    while(!meter_run_flag){
	sleep(0.1);
    }
    
    // reading loop
    int ret;
    while(meter_run_flag)
    {
	ret = sht31_measure(&s_sht31);
	if(ret != SHT31_READ_OK)
	{
	    printf("SHT31 read error!\n");
	    sht31_run_flag = 0;
            while(sht31_config(&s_sht31) != SHT31_CONFIG_OK){
                sleep(0.1);
            }
	}
	else{
            sht31_run_flag = 1;
	    printf("************* SHT31 task ************* temp=%fC, RH=%f\n", s_sht31.temp, s_sht31.humidity);
	}
	sleep(5);
    }
}

/*
 *
 * Task : thread_redis_subscribe()
 * Description : Redis subscribe for Relay control
 *
 */
void *thread_redis_subscribe(void *arg)
{
    /*
     * REDIS Async Initialize
     *
     */  
    signal(SIGPIPE, SIG_IGN);
    struct event_base *base = event_base_new();

    // Create Redis connect configuration
    redisOptions options = {0};
    REDIS_OPTIONS_SET_TCP(&options, REDIS_URL, REDIS_PORT);

    struct timeval tv = {0};
    tv.tv_sec = 1;
    options.connect_timeout = &tv;
    
    // wait for measurement task to start running, REDIS server should be ready
    while(!meter_run_flag)
    {
	 delay(10);
    }

redis_async_init:
    printf("REDIS Async Initialize...\n");

    /*
     *  REDIS Async Initialized Connection
     *  This will try to reconnect REDIS server for every 5 second
     */

    redisAsyncContext *c = redisAsyncConnectWithOptions(&options);
    while (c->err) {
        /* Let *c leak for now... */
        printf("Error: %s\n", c->errstr);

	// wait for 2 second then reconnect
        sleep(2);
	c = redisAsyncConnectWithOptions(&options);
    }
    printf("REDIS Async Initialize COmplete\n");
    redisLibeventAttach(c,base);
    
    // REDIS Connect, Disconnect Callback
    redisAsyncSetConnectCallback(c, connectCallback);
    redisAsyncSetDisconnectCallback(c, disconnectCallback);

    /*
     *  REDIS Authentication
     *
     */
    printf("REDIS Async : Send Authentication CMD\n");
    redisAsyncCommand(c, getCallback, NULL, "auth ictadmin");

    /*
     *  REDIS Subscribe
     *  This will go on forever
     */
    char *channel = (char *)REDIS_CHANNELNAME_RELAY_WRITE;
    printf("REDIS Async : Send Subscribe for relay tag\n");
    redisAsyncCommand(c, onMessage, NULL, "SUBSCRIBE tag:%s", channel);
    event_base_dispatch(base);



    //loop for checking redis async connection every 10 seconds
    while(meter_run_flag)
    {
	if(c->err || (!redis_async_connected))
	{
	    redisAsyncFree(c);
	    printf("Reconnecting REDIS Async...\n");
            goto redis_async_init;
	}
	sleep(10);
    }
}

/*
 *
 * Task : thread_watthour_pulse()
 * Description : Generate energy pulse from measurement data
 * Warning : This task is already deprecated due to change to use EPG of cs5484 instead! 
 *
 */
void *thread_watthour_pulse(void *arg)   // This will be deprecated due to not be able to handle large load that has pulse period less than 1 second.
{
    double wh_previous;
    double varh_previous;
    int diff_wh;
    int diff_varh;
    int pulse_cnt_logging_kwh=0;
    int pulse_cnt_logging_kvarh=0;
    FILE *pulse_log_file;

    while(!meter_run_flag){
	// wait until meter start to run and start conversion
    }

    // initialize previous energy value
    wh_previous   = s_tag.wh;
    varh_previous = s_tag.varh;

    while(meter_run_flag){
	diff_wh   = (int)((s_tag.wh - wh_previous) * ENERGY_DIFF_MUL);     // difference wh multiply by constant 
	diff_varh = (int)((s_tag.varh - varh_previous) * ENERGY_DIFF_MUL); // difference varh multiply by constant
        	
	// check pulse for kwh
	if(diff_wh  >= (int)(ENERGY_DIFF_MUL / PULSE_PER_WH)){
            pulse_cnt_logging_kwh++;
	    printf("blink kWh!, pulse cnt (kWh)=%d\n", pulse_cnt_logging_kwh);
	    wh_previous = s_tag.wh;
	    led_kwh_pulse(&s_relay_config);

#if DATALOGGING_ENERGY_PULSE_TEST
	    /*
	     *  Stamp blinking to log file
	     *
	     */
            char timestamp[30];
	    rtc_getTime(timestamp);
            pulse_log_file = fopen(PULSE_LOG_FILENAME, "a");
	    if(pulse_log_file != NULL){
               char str_buf[200];
	       sprintf(str_buf, "blink kWh!. w=%f, pulse_kwh=%d, pulse_kvarh=%d,timestamp=%s\n", 
		       s_tag.p, pulse_cnt_logging_kwh, pulse_cnt_logging_kvarh, timestamp);

	       fputs(str_buf, pulse_log_file);
	       fclose(pulse_log_file);
	   }
#endif
        }

	// check pulse for kvarh
	if(diff_varh  >= (int)(ENERGY_DIFF_MUL / PULSE_PER_VARH)){
            pulse_cnt_logging_kvarh++;
	    printf("blink kVARh!, pulse cnt (kVARh)=%d\n", pulse_cnt_logging_kvarh);
	    varh_previous = s_tag.varh;
            led_kvarh_pulse(&s_relay_config);
	    
#if DATALOGGING_ENERGY_PULSE_TEST
	    /*
	     *  Stamp blinking to log file
	     *
	     */
            char timestamp[30];
	    rtc_getTime(timestamp);
            pulse_log_file = fopen(PULSE_LOG_FILENAME, "a");
	    if(pulse_log_file != NULL){
               char str_buf[200];
	       sprintf(str_buf, "blink kVARh!. pulse_kwh=%d, pulse_kvarh=%d,timestamp=%s\n", pulse_cnt_logging_kwh, pulse_cnt_logging_kvarh, timestamp);
	       fputs(str_buf, pulse_log_file);
	       fclose(pulse_log_file);
	    }
#endif
	}
    }
}	

/*
 *
 * Task : thread_energy_accumulate()
 * Description : Calculate accumulated energy from energy pulse of CS5484 EPG function block
 *
 */
void *thread_energy_accumulate()
{
    uint32_t previous_pulse_cnt_kwh;
    uint32_t previous_pulse_cnt_kvarh;
    uint32_t tmp1;
    uint32_t tmp2;
    double dkwh;
    double dkvarh;

    while(!meter_run_flag)
    {    
	// do nothing, wait for meter_run_flag
	sleep(0.1);
    }

    // reset pulse count as task measurement() is running
    pulse_cnt_kwh=0;
    pulse_cnt_kvarh=0;
    previous_pulse_cnt_kwh   = 0;
    previous_pulse_cnt_kvarh = 0;

    while(meter_run_flag)
    {
        // Sample pulse count value
        tmp1 = pulse_cnt_kwh;
        tmp2 = pulse_cnt_kvarh;
        
	// Calculate energy kWh
	if (tmp1 != previous_pulse_cnt_kwh)
	{
	    // check overflow
	    if(tmp1 - previous_pulse_cnt_kwh < 0){
	        dkwh = (double)(0xFFFFFFFF - previous_pulse_cnt_kwh + tmp1) / PULSE_PER_KWH;
	    }
	    else{
	        dkwh = (double)(tmp1 - previous_pulse_cnt_kwh) / PULSE_PER_KWH;
	    }

	    // Update energy kWh, kVARh
            s_tag.kwh  = s_tag.kwh + dkwh;
	    previous_pulse_cnt_kwh = tmp1;
	}

	// Calculate energy kVARh
	if (tmp2 != previous_pulse_cnt_kvarh)
	{
	    // check overflow
	    if(tmp2 - previous_pulse_cnt_kvarh < 0){
	        dkvarh = (double)(0xFFFFFFFF - previous_pulse_cnt_kvarh + tmp2) / PULSE_PER_KVARH;
	    }
	    else{
	        dkvarh = (double)(tmp2 - previous_pulse_cnt_kvarh) / PULSE_PER_KVARH;
	    }

	    // Update energy kWh, kVARh
            s_tag.kvarh  = s_tag.kvarh + dkvarh;
	    previous_pulse_cnt_kvarh = tmp2;
	}

	sleep(0.05);
    }
}

/*
 *
 * Task : thread_internet_connection()
 * Description : check internet connection of meter and save as flag for REDIS task to set connect status tag.
 *
 */
void *thread_internet_connection()
{
    while(!meter_run_flag)
    {   
	sleep(0.1);   
	// do nothing, wait for meter_run_flag
    }

    while(meter_run_flag)
    {
        /* 
	 * Use Ping to google DNS to check network connection
	 *
	 */  
        int ping_ret;
        ping_ret = system("ping -c1 -w1 8.8.8.8 > /dev/null 2>&1");
        if(ping_ret == 0){
            printf("*\n*\n Ping Success, Online\n*\n*\n");
	    s_tag.internet_status = INTERNET_STATUS_CONNECTED;
	}
        else{
            printf("*\n*\nPing Failed, Offline\n*\n*\n");
	    s_tag.internet_status = INTERNET_STATUS_DISCONNECTED;
	}

	sleep(30); // soft sleep for about 30 sec
    }
}

/*
 *
 * Task : thread_sleep_lcd()
 * Description : Dim LCD's backlight when meter is in the sleep mode
 *
 */
void *thread_sleep_lcd()
{
        int fd;
        char buf[256];
        fd_set readfds;
        struct input_event event;
	struct timeval tv;

	/* 
	 * Initialize LCD's backlight
	 */ 
	system("echo 255 | sudo tee /sys/class/backlight/rpi_backlight/brightness");
	sleep(60);

reinit:
	/* 
	 * Open input device
	 */
	do {
        	fd = open("/dev/input/mice", O_RDONLY);
		sleep(3);
	}
	while(fd == -1);

        /* 
	 * Set fd to wait for input
	 */
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);

	/*
	 * Touch check loop
	 * For sleep and wakeup scheduling
	 *
	 */
	int touch_state;
	int count;
        int ret;
	tv.tv_sec = 61;
	tv.tv_usec = 0;
        while(1){
                ret = select(fd+1, &readfds, NULL, NULL, &tv);
                if(ret > 0){
			printf("touch is detected!\n");
			system("echo 255 | sudo tee /sys/class/backlight/rpi_backlight/brightness");
			close(fd);
			sleep(0.1);
			goto reinit;
                }
		else{
			printf("touch timeout...\n");
			system("echo 0 | sudo tee /sys/class/backlight/rpi_backlight/brightness");
			close(fd);
			sleep(0.1);
			goto reinit;
		}
		sleep(1);
        }

        return 0;
}



/*
 *
 * Main program
 *
 *
 */
int main(){
    /*
     *  Create threads
     *  - Thread for measurement loop
     *  - Thread for Redis subscribe callback in Relay Control & Other write method by client
     *  - Thread for meter pulse
     *  - Thread for internet connection status
     *  - Thread for LCD backlight sleep control 
     */
    pthread_t t_measure;
    pthread_t t_redis_sub;
    pthread_t t_pulse;
    pthread_t t_sht31;
    pthread_t t_internet;
    pthread_t t_lcd;
    pthread_create(&t_measure,   NULL, &thread_measurement,     NULL);
    pthread_create(&t_redis_sub, NULL, &thread_redis_subscribe, NULL);
    pthread_create(&t_pulse,     NULL, &thread_energy_accumulate,  NULL);
#if USE_SHT31
    pthread_create(&t_sht31,     NULL, &thread_sht31_temperature,  NULL);
#endif
    pthread_create(&t_internet,  NULL, &thread_internet_connection,  NULL);
    pthread_create(&t_lcd,       NULL, &thread_sleep_lcd,  NULL);

    // join all thread
    pthread_join(t_measure,   NULL);
    pthread_join(t_redis_sub, NULL);
    pthread_join(t_pulse, NULL);
#if USE_SHT31
    pthread_join(t_sht31, NULL);
#endif
    pthread_join(t_internet, NULL);
    pthread_join(t_lcd, NULL);

    return 0;
}



/*
 *
 *  User-Level Meter Functions
 *
 */
int meter_wiringpi_init(cs5484_basic_config_t *s_config, relay_led_config_t *s_relay_config, cs5484_epg_config_t *s_epg)
{
    // WiringPi setup
    wiringPiSetupGpio();
    wiringPiSPISetupMode(s_config->pi_spi_channel, s_config->spi_bus_speed, s_config->spi_mode);
    
    // Config GPIO for SPI bus
    pinMode(s_config->spi_cs_pin, OUTPUT);
    pinMode(s_config->chip_reset_pin, OUTPUT);
    pinMode(s_epg->kwh_pin, INPUT);
    pinMode(s_epg->kvarh_pin, INPUT);
    pullUpDnControl(s_config->spi_cs_pin, PUD_OFF);
    pullUpDnControl(s_config->chip_reset_pin, PUD_UP);
    pullUpDnControl(s_epg->kwh_pin, PUD_OFF);
    pullUpDnControl(s_epg->kvarh_pin, PUD_OFF);
    digitalWrite(s_config->chip_reset_pin, 1);
    digitalWrite(s_config->spi_cs_pin, 1);
    
    // Config GPIO for Relay & Pulse LED
    relay_led_gpio_init(s_relay_config);
    
    // Test blinking pulse LED
    /*
    led_kwh_on(s_relay_config);
    led_kvarh_on(s_relay_config);
    delay(3000);
    led_kwh_off(s_relay_config);
    led_kvarh_off(s_relay_config);
    */


    // Hard Reset CS5484
    digitalWrite(s_config->chip_reset_pin, 0);
    delay(250);
    digitalWrite(s_config->chip_reset_pin, 1);
    delay(250);

    // Soft Reset CS5484 (Reset all configuration)
    cs5484_reset(s_config);
    sleep(10);

    // GPIO interrupt for CS5484's EPG
    wiringPiISR(s_epg->kwh_pin,   INT_EDGE_FALLING, &EPGInterruptCallback_KWH);
    wiringPiISR(s_epg->kvarh_pin, INT_EDGE_FALLING, &EPGInterruptCallback_KVARH);

    return STATUS_OK;
}

int meter_chipmeasurement_init(cs5484_basic_config_t *s_cs5484_config,
		               cs5484_input_config_t *s_cs5484_channel_1,
			       cs5484_input_config_t *s_cs5484_channel_2,
			       cs5484_epg_config_t   *s_cs5484_epg)
{   
    	
    // set AC offset filter
    cs5484_input_filter_init(s_cs5484_config, s_cs5484_channel_1);

    // enable temperature sensor
    cs5484_temperature_enable(s_cs5484_config, 1);

    // set I1gain, V1gain
    cs5484_set_gain_voltage(s_cs5484_config, s_cs5484_channel_1);
    cs5484_set_gain_current(s_cs5484_config, s_cs5484_channel_1);

    // set Iacoff
    cs5484_set_offset_current(s_cs5484_config, s_cs5484_channel_1);

    // set phase compensation 
    cs5484_set_phase_compensation(s_cs5484_config, s_cs5484_channel_1);
    
    // set EPG configuration
    cs5484_epg_setup(s_cs5484_config, s_cs5484_epg);

    // record user config for config0, config1, config2 for chip config loss diagnose
    cs5484_page_select(s_cs5484_config, 0);
    cs5484_reg_read(s_cs5484_config, &meter_user_config0, 0);

    return 0;
}

int meter_check_chipconfig(cs5484_basic_config_t *s_cs5484_config, uint32_t ref_config0)
{
    uint32_t previous_config0;

    // Read a previous config value of Config0
    cs5484_page_select(s_cs5484_config, 0);
    cs5484_reg_read(s_cs5484_config, &previous_config0, 0);
    
    if(previous_config0 != ref_config0) return STATUS_FAIL;
    else return STATUS_OK;
}

int meter_read_backup_file(FILE **backup_file, const char *filename, double *field, int n_field)
{
    char line[1024];

    *backup_file = fopen(filename, "r");
    if(*backup_file == NULL){
        printf("backup file does not exist, exist program\n");
	return STATUS_READ_BACKUP_ERROR;
    }
    else{
        fgets(line, 1024, *backup_file);
        printf("read file : %s\n", line);
    	for(int i=0; i<n_field; i++){
            char *token;
            char tmp[1024];
            int len;
            int len_token;
       
	    // copy line to tmp buffer. because strtok() change input string
	    memcpy(tmp, line, sizeof(line));

	    // extract token 
            token = strtok(tmp, ",");

	    // convert string to floating-point value
	    field[i] = atof(token);

	    // update line by remove the previous token from the front of line string
	    len = strlen(line);
	    len_token = strlen(token);
            memcpy(tmp, line + len_token + 1, sizeof(line) - sizeof(len_token) - sizeof(char));
            memcpy(line, tmp, sizeof(tmp));
        }
        fclose(*backup_file);
    }

    return STATUS_READ_BACKUP_OK;
}

void meter_read_relay_polarity(FILE **relay_config_file, const char *filename, int *relay_polarity){
    
    char buffer[1024];
    struct json_object *parsed_json;
    struct json_object *polarity;

    *relay_config_file = fopen(filename, "r");
    if(*relay_config_file == NULL){
        printf("relay config file does not exist, exit read function\n");
    }
    else{
        fread(buffer, 1024, 1, *relay_config_file);
        fclose(*relay_config_file);

        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "relay_polarity", &polarity);
        *relay_polarity = json_object_get_int(polarity);
        
	printf("Relay Polarity=%d\n", *relay_polarity);
    }
}

void meter_load_calibration(FILE **calibration_file, const char *filename, 
		            cs5484_input_config_t *s_cs5484_channel_1)
{
    char line[1024];
    const int n_field = 6;
    double field[n_field];

    *calibration_file = fopen(filename, "r");
    if(*calibration_file == NULL){
        printf("calibration file does not exist, exit read function\n");
    }
    else{
	/*
	 *  This procedure should be replaced by fscanf() instead!
	 *
	 */
        fgets(line, 1024, *calibration_file);
        printf("calibration param : %s\n", line);
    	for(int i=0; i<n_field; i++){
            char *token;
            char tmp[1024];
            int len;
            int len_token;
       
	    // copy line to tmp buffer. because strtok() change input string
	    memcpy(tmp, line, sizeof(line));

	    // extract token 
            token = strtok(tmp, ",");

	    // convert string to floating-point value
	    if(i != 5) field[i] = (double)atoi(token);
	    else field[i] = atof(token);

	    // update line by remove the previous token from the front of line string
	    len = strlen(line);
	    len_token = strlen(token);
            memcpy(tmp, line + len_token + 1, sizeof(line) - sizeof(len_token) - sizeof(char));
            memcpy(line, tmp, sizeof(tmp));
        }
        fclose(*calibration_file);
    }

    s_cs5484_channel_1->gain_v = field[0];
    s_cs5484_channel_1->gain_i = field[1];
    s_cs5484_channel_1->ac_offset_i = field[2];
    s_cs5484_channel_1->offset_p = field[3];
    s_cs5484_channel_1->offset_q = field[4];
    s_cs5484_channel_1->phase_offset_manual = field[5];
    printf("****** load calibration *******\n");
    printf("%d\n", s_cs5484_channel_1->gain_v);
    printf("%d\n", s_cs5484_channel_1->gain_i);
    printf("%d\n", s_cs5484_channel_1->ac_offset_i);
    printf("%d\n", s_cs5484_channel_1->offset_p);
    printf("%d\n", s_cs5484_channel_1->offset_q);
    printf("%f\n", s_cs5484_channel_1->phase_offset_manual);
    printf("*******************************\n");
}

void meter_load_ct_profile(FILE **ct_file, const char *filename, ct_profile_t *ct_model)
{
    char buffer[1024];
    struct json_object *parsed_json;
    struct json_object *ct_name;
    struct json_object *model_type;
    struct json_object *degree;
    struct json_object *coeffs;
    struct json_object *coeff;
    struct json_object *intercept;
    size_t n_coeffs;

    *ct_file = fopen(filename, "r");
    if(*ct_file == NULL){
        printf("CT profile file does not exist, exit read function\n");
    }
    else{
        fread(buffer, 1024, 1, *ct_file);
        fclose(*ct_file);

        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "ct_name", &ct_name);
        json_object_object_get_ex(parsed_json, "model_type", &model_type);
        json_object_object_get_ex(parsed_json, "degree", &degree);
        json_object_object_get_ex(parsed_json, "coeffs", &coeffs);
        json_object_object_get_ex(parsed_json, "intercept", &intercept);
        
	printf("CT Profile Loading...\n");
        printf("CT name: %s\n", json_object_get_string(ct_name));
        printf("Model type: %s\n", json_object_get_string(model_type));
        printf("Degree: %d\n", json_object_get_int(degree));

        n_coeffs = json_object_array_length(coeffs);
        printf("Coeff lenght =  %lu\n", n_coeffs);

	// Config CT model struct with loaded data
        ct_model->model_type = json_object_get_int(model_type);
        ct_model->degree = json_object_get_int(degree);
        ct_model->intercept = json_object_get_double(intercept);
        for(int i=0;i<n_coeffs;i++) {
                coeff = json_object_array_get_idx(coeffs, i);
                ct_model->coeffs[i] = json_object_get_double(coeff);
                printf("%d. %lf\n", i+1, json_object_get_double(coeff));
        }
        printf("intercept=%lf\n", ct_model->intercept);
    }
}






/*
 * Redis Callback Functions
 *
 */

void connectCallback(const redisAsyncContext *c, int status) {
    if (status != REDIS_OK) {
        printf("Error: %s\n", c->errstr);
    }
    else{
        printf("REDIS async Connected...\n");
	redis_async_connected = 1;
    }
}

void disconnectCallback(const redisAsyncContext *c, int status) {
    if (status != REDIS_OK) {
        printf("Error: %s\n", c->errstr);
    }
    else{
        printf("REDIS async Disconnected...\n");
	redis_async_connected = 0;
    }
}

void onMessage(redisAsyncContext * c, void *reply, void * privdata) {
    int j;
    redisReply * r = reply;
    if (reply == NULL) return;

    printf("got a message of type: %i\n", r->type);
    if (r->type == REDIS_REPLY_ARRAY) {
        for (j = 0; j < r->elements; j++) {
            printf("%u) %s\n", j, r->element[j]->str);
          
	    // Extract write value of relay control from client
            int write_val = 0;
	    if((j == r->elements - 1) && (r->element[j]->str != NULL)){
                write_val = atoi(r->element[j]->str);
                if(write_val == RELAY_STATE_DISCONNECT){
	            printf("Disconnect latching relay\n");
                    relay_disconnect(&s_relay_config);
		    s_tag.relay_state = RELAY_STATE_DISCONNECT;
                }
                else{
	            printf("Connect latching relay\n");
                    relay_connect(&s_relay_config);
		    s_tag.relay_state = RELAY_STATE_CONNECT;
                }
	    }
        }
    }
}

void getCallback(redisAsyncContext *c, void *r, void *privdata) {
    redisReply *reply = r;
    if (reply == NULL) return;
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);
}

/*
 * Energy Pulse Callback function
 *
 */
void EPGInterruptCallback_KWH(){
    printf("********** KWH Pulse! **********\n");
    led_kwh_pulse(&s_relay_config);
    pulse_cnt_kwh++;
}
void EPGInterruptCallback_KVARH(){
    printf("********** KVARH Pulse! **********\n");
    led_kvarh_pulse(&s_relay_config);
    pulse_cnt_kvarh++;
}
